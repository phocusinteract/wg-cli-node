/**
 * Configuration file for the wg-cli
 *
 * @key WG_REPOSITORY {Sring}      - The Widgrid Core Repository
 * @key WG_MODULES_TARGET {String} - Default target dir to install modules (relative to the curr dir)
 */
module.exports = {
  WG_REPOSITORY: "https://felippemoraes@bitbucket.org/phocusinteract/widgrid.git",
  MODULES_TARGET: "plugins/Widget/src/Widget",
}
