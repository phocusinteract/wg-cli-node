/*
 * Paths configuration for wg-cli enviroment.
 * Paths must be Abs or relative to the root.
 * DO NOT include the trailing slashes ("/")
 *
 * @type Json  
 */
const path = require('path')
const APP  = path.dirname(require.main.filename)

module.exports = {
  ROOT:       APP,
  CMD:        `${APP}/cmd`,
  CONFIG:     `${APP}/config`,
  LIB:        `${APP}/lib`,
  MODULES:    `${APP}/wg_modules`
}