/**
 * Default .wgconfig object. Will be cloned when
 * you run wg init. So, change this object to
 * change the default .wgconfig result on wg init.
 * Change the configuration on /cmd/init/index.js
 *
 * @type Json
 */
module.exports =  {
  "title": "",
  "url": "",
  "created": "",
  "version": "",
  "description": "",
  "modules": [],
  "repository": "",
  "installed": [],
}
