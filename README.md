# WG CLI

This is the Widgrid BETA command line interface. 

# Getting started

Go to the wg-cli folder and run

```bash
npm install -g
```

# Basic Usage:

```bash
wg <command> [-args] {options}
```

# TODO

FUNCTION WHICH DOWNLOADS ALL THE MODULES AND CONTENTS FROM A GIVEN WGCONFIG
USE THIS FUNCTION AT THE END OF THE wg init COMMAND FOR WHILE'

wg install: install all the modules from the .wgconfig,
wg install {module}: install an specific module and adds it to the .wgconfig,
wg unlink {module}: uninstall all modules, .wgconfig file and clears everything

This will create a .wgconfig file on your project root. 
This has simple json inside wich represents your project state.

# Research

CREATING A REAL-WORLD CLI APP WITH NODE
https://timber.io/blog/creating-a-real-world-cli-app-with-node/

Building command line tools with Node.js
https://developer.atlassian.com/blog/2015/11/scripting-with-node/

Build a JavaScript Command Line Interface (CLI) with Node.js
https://www.sitepoint.com/javascript-command-line-interface-cli-node-js/