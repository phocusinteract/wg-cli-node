/**
 * Help output for "wg h"
 *
 * @example
 * // bash
 * wg h
 */
module.exports = () => {
  console.log(`
wg <command> [args] {options}

help          Shows this help message
clone         Clones a new Widgrid Core Project
create        Create a new Project | clone -> init -> install
init          Starts a new project config on the current folder
install       Installs the wg modules based on .wgconfig file
update-cli    Updates this cli
  `);
}
