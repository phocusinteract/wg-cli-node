const color       = require('chalk');
const shell       = require('shelljs');

module.exports = (context) => {

  if(context.args[0] == 'help'){
    require('./help')();
    return;
  }

  let cli_dir = __basedir;

  // git update using shell
  if (!shell.which('git')) {
    console.log(color.red('Git not available on your project. Cannot clone.'))
    return;
  } else {
    console.log(color.green('Updating your Wg-Cli, this may take a while ...'))
    shell.exec(`cd ${cli_dir} && git pull --progress 2>&1`);
  }
}
