/**
 * Help output for "wg clone help"
 */
module.exports = () => {
  console.log(`
Usage: wg update-cli
About: Use it to update you wg-cli

Params:

help         Shows this help message
  `);
}
