
const color          = require('chalk');
const paths          = require('./../../config/paths')

const warns          = require(`${paths.LIB}/warns`);
const call           = require(`${paths.LIB}/caller`).call;
const readInputSync  = require(`${paths.LIB}/helpers`).readInputSync;
const createJsonFile = require(`${paths.LIB}/helpers`).createJsonFile;
const hasWgConfig    = require(`${paths.LIB}/helpers`).hasWgConfig;
const hasPack        = require(`${paths.LIB}/helpers`).hasPack;
const getPack        = require(`${paths.LIB}/helpers`).getPack;
const listPacks      = require(`${paths.LIB}/helpers`).listPacks;

const config         = require(`${paths.CONFIG}/config`);
const packs          = require(`${paths.CONFIG}/packs`);
const _wgconfig      = require(`${paths.CONFIG}/wgconfig`);

const projectFactory = () => {
  return _wgconfig;
}
const criticalWarn = () => {
  warns.already_has_wg_config()
  return false;
}

/**
 * Wg Init Command
 *
 * This module will handle the necessary logic to
 * create a new .wgconfig file based on a passed
 * params. At end, will call the "install" module
 * over the new brand .wgconfig.
 *
 * @selectedPack {Object} String  - pack passed to the command
 */
module.exports = (context) => {

  // verify the first argument after the command
  const firstParam = context.args[0];

  if(hasWgConfig()){
    criticalWarn();
    return;
  }

  if(firstParam == 'help'){
    require('./help')();
    return;
  }

  if(firstParam == 'packs'){
    listPacks();
    return;
  }

  // if the firstArg was not a param (help or packs), its a
  // starter pack. If there is no firstParam, then must be
  // an empty Array (to match context.args) pattern
  const selectedPack = firstParam || [];

  // if there is a selectedPack and its not 'help' or 'packs'
  // then is always a starter pack request
  if(selectedPack.length && !hasPack(selectedPack)){
    console.log(color.red(`\nError: There is no pack named ${selectedPack}`))
    console.log('Type "wg init packs" to see the available start packs\n')
    return;
  }

  // Create an empty project and its pack
  let project = projectFactory();
  let pack;

  // Get the pack if have on
  if(selectedPack) pack = getPack(selectedPack);
  if(pack)  project.modules = pack;

  // Fill the created project
  project.created = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');

  console.log('\n');
  if(selectedPack.length) console.log(color.green(`Starter Pack selected: ${selectedPack}`)); else console.log(color.yellow('No Stater Pack selected!'));

  console.log('\n');
  project.title           = readInputSync(`Title (Untitled):`)   || 'Untitled';
  project.version         = readInputSync(`Version (1.0.0):`)    || '1.0.0';
  project.description     = readInputSync("Description (null):") || null;

  let choice = false;
  console.log(color.bold('\nPlease check the configuration below.\nIs everything right, do you want to proceed?\n'));
  while(!choice) choice = readInputSync("(y/n): ");
  if(!choice || choice.toUpperCase() != "Y") return;

  createJsonFile(project, '.wgconfig', './', () => {
    console.log(color.green(`\n* Created .wgconfig file\n`));
  });
}
